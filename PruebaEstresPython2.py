'''
Created on 6 dic. 2020

@author: Cesar
'''
from time import time
import random
from pip._internal.req.req_uninstall import compact

class Corrector:
    def correcion(self):
        e=1
        while e==1:
            try:
                r = int(input())
            except:
                print("Ups, solo numero enteros mayores a 0")
                e=1
            else:
                if r>0:
                    e=0
                else:
                    print("Ups, solo numeros entero mayores a 0")
                    e=1
        return r

class Intercalacion:
   
    
    
    @staticmethod
    def interc(a1:list, a2:list) -> list:
        intercambios = 0
        comparaciones = 0
        pasadas = 0
        a3 = []
        cont : int = 0
        cont2 : int = 0

        while len(a1)!=cont and len(a2)!=cont2:
            pasadas = pasadas +1
            if a1[cont]<=a2[cont2]:
                a3.append(a1[cont])
                cont+=1
                comparaciones = comparaciones +1
            else:
                a3.append(a2[cont2])
                intercambios = intercambios +1
                cont2+=1

        while len(a1)!=cont:
            a3.append(a1[cont])
            intercambios = intercambios +1
            cont+=1

        while len(a2)!=cont2:
            pasadas = pasadas +1
            a3.append(a2[cont2])
            cont2+=1
        print(f"Numerode intercambios = {intercambios}")
        print(f"Numero de comparaciones = {comparaciones}")
        print(f"Numero de pasadas = {pasadas}")
        return a3
    
    def aplicarI(self,v1,v2):
        tInicio = time()
        m = MezclaDirecta()
        vv1 = []
        vv2 = []
        vv1 = m.mezclaDirecta(v1)
        vv2 = m.mezclaDirecta(v2)
        v = []
        v = self.interc(vv1, vv2)
        tiempo = time()-tInicio
        print(f"Tiempo de ejecucion =  {tiempo}")
                   
        return v
        
    
    
class MezclaDirecta:
    pasadas =0
    comparaciones = 0
    intercambios =0
    def aplicarD(self,numeros):
        tInicio = time()
        self.mezclaDirecta(numeros)
        tiempo = time()-tInicio
        print(f"Tiempo de ejecucion =  {tiempo}")
        print(f"Numerode intercambios = {self.intercambios}")
        print(f"Numero de comparaciones = {self.comparaciones}")
        print(f"Numero de pasadas = {self.pasadas}")
        self.intercambios = 0
        self.comparaciones = 0
        self.pasadas = 0        

    
    
    def mezclaDirecta(self,numeros):
        mitad=len(numeros)//2
        self.comparaciones = self.comparaciones +1
        if len(numeros)>=2:
            arregloIz=numeros[mitad:]
            arregloDer=numeros[:mitad]
            self.pasadas = self.pasadas+1

            numeros.clear()  
            
            self.mezclaDirecta(arregloIz)
            
            self.mezclaDirecta(arregloDer)
            
            
            while(len(arregloDer)>0 and len(arregloIz)>0):
                self.comparaciones = self.comparaciones+1
                if(arregloIz[0]< arregloDer[0]):
                    numeros.append(arregloIz.pop(0))
                    self.intercambios = self.intercambios +1
                else:
                    numeros.append(arregloDer.pop(0))
                    self.intercambios = self.intercambios +1
                    self.pasadas = self.pasadas+1
            while len(arregloIz)>0:
                self.intercambios = self.intercambios +1
                numeros.append(arregloIz.pop(0))
                self.pasadas = self.pasadas+1
            
            while len(arregloDer)>0:
                self.intercambios = self.intercambios +1
                numeros.append(arregloDer.pop(0))
                self.pasadas = self.pasadas+1
        
        return numeros
    
    def mezclaDirecta2(self,numeros):
        mitad=len(numeros)//2
        self.comparaciones = self.comparaciones +1
        if len(numeros)>=2:
            arregloIz=numeros[mitad:]
            arregloDer=numeros[:mitad]
            self.pasadas = self.pasadas+1

            numeros.clear()  
            
            self.mezclaDirecta(arregloIz)
            
            self.mezclaDirecta(arregloDer)
            
            
            while(len(arregloDer)>0 and len(arregloIz)>0):
                self.comparaciones = self.comparaciones+1
                if(arregloIz[0]< arregloDer[0]):
                    numeros.append(arregloIz.pop(0))
                    self.intercambios = self.intercambios +1
                else:
                    numeros.append(arregloDer.pop(0))
                    self.intercambios = self.intercambios +1
                    self.pasadas = self.pasadas+1
            while len(arregloIz)>0:
                self.intercambios = self.intercambios +1
                numeros.append(arregloIz.pop(0))
                self.pasadas = self.pasadas+1
            
            while len(arregloDer)>0:
                self.intercambios = self.intercambios +1
                numeros.append(arregloDer.pop(0))
                self.pasadas = self.pasadas+1
        
       
        
class MezclaNatural:
    pasadas = 0
    intercambios = 0
    comparaciones =0
    def aplicarN(self,numeros):
        tInicio = time()
        self.mezclaNatural(numeros)
        tiempo = time()-tInicio
        print(f"Tiempo de ejecucion =  {tiempo}")
        print(f"Numerode intercambios = {self.intercambios}")
        print(f"Numero de comparaciones = {self.comparaciones}")
        print(f"Numero de pasadas = {self.pasadas}")
        self.intercambios = 0
        self.comparaciones = 0
        self.pasadas = 0
        
        
    def mezclaNatural(self,numeros):
        m = MezclaDirecta()
        izquerdo = 0
        izq = 0
        derecho = len(numeros)-1
        der = derecho
        ordenado=False
        
        while(not ordenado):
            ordenado = True
            izquierdo =0
            while(izquierdo<derecho):
                izq=izquerdo
                while(izq < derecho and numeros[izq]<=numeros[izq+1]):
                    izq=izq+1
                    self.pasadas = self.pasadas+1
                der=izq+1
                while(der==derecho-1 or der<derecho and numeros[der]<=numeros[der+1]):
                    der=der+1
                    self.pasadas = self.pasadas+1
                self.comparaciones = self.comparaciones+1
                if(der<=derecho):
                    m.mezclaDirecta2(numeros)
                    ordenado= False
                izquierdo = izq
                self.pasadas = self.pasadas+1
            self.pasadas = self.pasadas+1
    
d = MezclaDirecta()
n = MezclaNatural()
i = Intercalacion()
c = Corrector()

opcion = 0
ooo =0
while(ooo!=1 and ooo!=2):
    print("Digite 1 para usar un vector con 1000 datos")
    print("Digite 2 para usar un vector con 10000 datos")
    ooo=c.correcion()

numeros = []
numerosx = []
if(ooo==1):
    for x in range(1000):
        numeros.append(random.randint(0,1000))
        numerosx.append(random.randint(0,1000))
elif(ooo==2):
    for x in range(10000):
        numeros.append(random.randint(0, 1000))
        numerosx.append(random.randint(0,1000))
elif(ooo==3):
    for x in range(100000):
        numeros.append(random.randint(0, 1000))
        numerosx.append(random.randint(0,1000))

while(opcion != 4):
    n = numeros.copy()
    n2 = numerosx.copy()
    print("======================== MENU =========================")
    print("Digite 1 pasa usar el metodo de ordenamiento Intercalacion")
    print("Digite 2 para usar el metodo de ordenamiento MEzcla Directa")
    print("Digite 3 para usar el metodo de ordenamiento Mezcla Naturla")
    print("Digite 4 para ***SALIR***")
    opcion = c.correcion()
    
    if(opcion == 1):
        print("===== Metodo de Intercalacion ====")
        print("Vectores DEsordenados")
        print(f"{n}")
        print(f"{n2}")
        print("====================================0")
        v = i.aplicarI(n, n2)
        print("Vectores ordenados")
        print(f"{v}")
    elif(opcion == 2):
        print("===== MEtodo de mezcla Directa=====")
        print("Vector desordenado")
        print(f"{n}")
        d.aplicarD(n)
        print("Vector Ordenado")
        print(f"{n}")
    elif(opcion==3):
        print("===== MEtodo de mezcla Naturla=====")
        print("Vector desordenado")
        print(f"{n}")
        d.aplicarD(n)
        print("Vector Ordenado")
        print(f"{n}")
    else:
        print("Gracias por usar")


